# Requirements
- PHP 7.0
- MySQL
- Composer

# How to run locally
- Clone this repository with `git clone`
- Install app dependencies with `composer install`
- Run with `php -S localhost:PORT` or under your local web server

# How to update from repository
- run `git pull origin master`
