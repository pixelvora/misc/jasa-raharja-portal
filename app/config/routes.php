<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'auth/viewLogin';
$route['dashboard'] = 'dashboard/viewDashboard';
$route['dashboard/news/(:any)'] = 'dashboard/viewNews/$1';

$route['manage/users'] = 'users/view_table';
$route['manage/users/form'] = 'users/view_form';
$route['manage/users/action(:any)'] = 'users/action$1';
$route['manage/users/action(:any)/(:any)'] = 'users/action$1/$2';
$route['manage/users/action(:any)/(:any)/(:any)'] = 'users/action$1/$2/$3';

$route['manage/apps'] = 'apps/view_table';
$route['manage/apps/form'] = 'apps/view_form';
$route['manage/apps/form/(:any)'] = 'apps/view_form/$1';
$route['manage/apps/detail/(:any)'] = 'apps/view_detail/$1';
$route['manage/apps/action(:any)'] = 'apps/action$1';
$route['manage/apps/action(:any)/(:any)'] = 'apps/action$1/$2';
$route['manage/apps/action(:any)/(:any)/(:any)'] = 'apps/action$1/$2/$3';

$route['manage/news'] = 'news/view_table';
$route['manage/news/form'] = 'news/view_form';
$route['manage/news/form/(:any)'] = 'news/view_form/$1';
$route['manage/news/detail/(:any)'] = 'news/view_detail/$1';
$route['manage/news/action(:any)'] = 'news/action$1';
$route['manage/news/action(:any)/(:any)'] = 'news/action$1/$2';
$route['manage/news/action(:any)/(:any)/(:any)'] = 'news/action$1/$2/$3';
