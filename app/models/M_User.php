<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends JR_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }

    public function Get($by, $value = null)
    {
        switch ($by) {

            case 'role':
                return $this->GetByRole($value);
                break;

            case 'id':
                return $this->GetById($value);
                break;

            case 'username':
                return $this->GetByUsername($value);
                break;

            case 'npp':
                return $this->GetByNPP($value);
                break;
            
            default:
                break;
        }
        # code...
    }

    public function GetByRole($roleId)
    {
        $where = [
            'role_id' => $roleId 
        ];
        $this->db->select('users.*');
        $this->db->from('users_roles');
        $this->db->join('users', 'users.id = users_roles.user_id');
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function GetByUsername($username)
    {
        $where = [
            'username' => $username
        ];
        return $this->db->get_where('users', $where)->row();
    }

    public function GetByNPP($npp)
    {
        $where = [
            'npp' => $npp
        ];
        return $this->db->get_where('users', $where)->row();
    }

    public function GetById($userId)
    {
        $where = [
            'id' => $userId
        ];
        return $this->db->get_where('users', $where)->row();
    }

    public function GetRole($userId)
    {
        $where = [
            'user_id' => $userId 
        ];
        $this->db->select('roles.*');
        $this->db->from('users_roles');
        $this->db->join('roles', 'roles.id = users_roles.role_id');
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function Create($data)
    {
        $resultUserNPP = $this->GetByNPP($data['npp']);
        if($resultUserNPP) {
            $this->result->error_message = 'User dengan NPP '.$data['npp'].' sudah ada dalam database';
        } else {
            $this->db->set($data);
            $this->db->insert($this->table);
            $this->CreateRole($data['id'], 1);
            $this->result->data = true;
        }
        return $this->result;
    }

    public function Delete($userId)
    {
        $resultUser = $this->GetById($userId);
        if(!$resultUser) {
            $this->result->error_message = 'User tidak ditemukan';
        } else {
            $where = [
                'id' => $userId
            ];
            $this->db->where($where);
            $this->db->delete($this->table);
            $this->DeleteRole($userId);
            $this->result->data = true;
        }
        return $this->result;
    }

    public function CreateRole($userId, $roleId)
    {
        $data = [
            'user_id' => $userId,
            'role_id' => $roleId
        ];
        $this->db->set($data);
        $this->db->insert('users_roles');
    }

    public function DeleteRole($userId)
    {
        $where = [
            'user_id' => $userId
        ];
        $this->db->where($where);
        $this->db->delete('users_roles');
    }

    public function Update($data, $userId)
    {
        $where = [
            'id' => $userId
        ];
        $this->db->where($where);
        $this->db->set($data);
        $this->db->update('users');
    }

}

/* End of file M_User.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/models/M_User.php */