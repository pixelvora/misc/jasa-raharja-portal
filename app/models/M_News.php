<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_News extends JR_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'news';
        $this->load->model('M_User', 'User');
    }

    public function Get($id = null)
    {
        if($id) {
            $where = [
                'id' => $id
            ];
            $result = $this->db->get_where($this->table, $where)->row();
            $result->user = $this->User->Get('id', $result->created_by);
            return $result;
        } else {
            $result = $this->db->get($this->table)->result();
            return array_map(function($n){
                $n->user = $this->User->Get('id', $n->created_by);
                return $n;
            }, $result);
        }
    }

    public function Create($data)
    {
        $this->db->insert($this->table, $data);
        return $this->ReturnStatus();
    }

    public function Update($data, $id)
    {
        $where = [
            'id' => $id
        ];
        $this->db->where($where);
        $this->db->set($data);
        $this->db->update($this->table);
        return $this->ReturnStatus();
    }

    public function Delete($id)
    {
        $where = ['id' => $id];
        $this->db->delete($this->table, $where);
        return $this->ReturnStatus();
    }

}

/* End of file M_News.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/models/M_News.php */