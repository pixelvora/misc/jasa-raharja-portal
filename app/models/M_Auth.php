<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Auth extends JR_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function GetLogin($userId)
    {
        $where = [
            'user_id' => $userId
        ];
        return $this->db->get('logins', $where)->row();
    }

    public function LoginREST($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $url = "http://rest.jasaraharja.co.id/index.php/adjr?email={$username}&password={$password}&key=UserJr1961";
        return json_decode(file_get_contents($url));
    }

}

/* End of file M_Auth.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/models/M_Auth.php */