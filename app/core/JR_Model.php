<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JR_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->result = new stdClass();
    }

    public function ReturnStatus()
    {
        if($this->db->affected_rows()) {
            $this->result->data = true;
        } else {
            $this->result->error_message = true;
        }
        // debug($this->result);
        return $this->result;
    }

}

/* End of file JR_Model.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/core/JR_Model.php */