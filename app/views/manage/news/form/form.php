<form method="POST" enctype='multipart/form-data' action="<?php echo base_url($cur . '/action' . ucfirst($action)) ?>">
  <?php if($action == 'update'): ?>
    <input type="hidden" name="id" value="<?php echo $id ?>">
  <?php endif; ?>

  <div class="form-group">
    <label>Judul Berita</label>
    <input type="text" name="title" class="form-control" placeholder="Judul Berita" required value="<?php echo @$news->title ?>"/>
  </div>

  <div class="form-group">
    <label>Konten Berita</label>
    <textarea class="form-control" name="content" cols="30" rows="10" placeholder="Konten Berita"><?php echo @$news->content ?></textarea>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-success mr-2">
      Simpan Data
    </button>
    <a href="<?php echo base_url($cur) ?>" class="btn btn-outline-secondary">Batal</a>
  </div>
</form>