<table class="table datatables" cellspacing="0">
  <thead>
    <tr>
      <th>Judul</th>
      <th>Tanggal</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($news as $key => $n): ?>
    <tr>
      <td style="width: 480px">
        <a target="_blank" href="<?php echo base_url('dashboard/news/') . @$n->id ?>">
          <?php echo @$n->title ?>
        </a>
      </td>
      <td><?php echo carbonFormat($n->created_at, 'd M Y H:i:s') ?></td>
      <td>
        <a href="<?php echo base_url($cur . '/form/') . $n->id ?>" class="btn btn-outline-primary">
          <i class="fa fa-pencil"></i> Update
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>