<?php if($this->session->flashdata('alert')) echo $this->session->flashdata('alert'); ?>

<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data User Admin
      <a href="<?php echo base_url($cur . '/form/') ?>" class="btn btn-primary btn-fw pull-right">
        <i class="fa fa-plus"></i> Tambah User Admin Baru
      </a>
    </h4>
    <div class="row">
      <div class="col-12">
        <?php $this->load->view($cur . '/view/table'); ?>
      </div>
    </div>
  </div>
</div>