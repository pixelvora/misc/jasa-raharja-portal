<form method="POST" action="<?php echo base_url($cur . '/action' . ucfirst($action)) ?>">

  <div class="form-group">
    <label>NPP</label>
    <input type="text" name="npp" class="form-control" placeholder="Isikan NPP Pegawai" required />
  </div>
  
  <div class="form-group">
    <button type="submit" class="btn btn-success mr-2">
      Simpan Data
    </button>
    <a href="<?php echo base_url($cur) ?>" class="btn btn-outline-secondary">Batal</a>
  </div>
</form>