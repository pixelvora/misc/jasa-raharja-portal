<table class="table datatables" cellspacing="0">
  <thead>
    <tr>
      <th>Nama Aplikasi</th>
      <th>URL</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($apps as $key => $app): ?>
    <tr>
      <td><?php echo @$app->name  ?></td>
      <td>
        <a target="_blank" href="<?php echo @$app->url ?>">
          <?php echo @$app->url ?>
        </a>
      </td>
      <td>
        <!-- <a href="<?php echo base_url($cur . '/detail/') . $app->id ?>" class="btn btn-outline-secondary">
          Detail
        </a> -->
        <a href="<?php echo base_url($cur . '/form/') . $app->id ?>" class="btn btn-outline-primary">
          <i class="fa fa-pencil"></i> Update
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>