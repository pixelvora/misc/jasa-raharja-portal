<form method="POST" enctype='multipart/form-data' action="<?php echo base_url($cur . '/action' . ucfirst($action)) ?>">
  <?php if($action == 'update'): ?>
    <input type="hidden" name="appId" value="<?php echo $id ?>">
  <?php endif; ?>

  <div class="form-group">
    <label>Nama Aplikasi</label>
    <input type="text" name="name" class="form-control" placeholder="Nama Aplikasi" required value="<?php echo @$app->name ?>"/>
  </div>

  <div class="form-group">
    <label>URL Aplikasi</label>
    <input type="text" name="url" class="form-control" placeholder="URL Aplikasi (http://ke.url.aplikasi)" required value="<?php echo @$app->url ?>"/>
  </div>

  <div class="form-group">
    <label>Upload Gambar Pendukung</label>
    <?php if($action == 'update'): ?>
      <?php if(@$app->image): ?>
        <br>
        <img style="width: 240px" src="<?php echo $app->image->url ?>" alt="">
        <br>
        <br>
      <?php else: ?>
        <div class="alert alert-info">Gambar Tidak Ditemukan</div>
      <?php endif; ?>
    <?php endif; ?>
    <input type="file" name="image" class="file-upload-default">
    <div class="input-group col-xs-12">
      <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
      <span class="input-group-append">
        <button class="file-upload-browse btn btn-info" type="button">Pilih Gambar</button>
      </span>
    </div>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-success mr-2">
      Simpan Data
    </button>
    <a href="<?php echo base_url($cur) ?>" class="btn btn-outline-secondary">Batal</a>
  </div>
</form>