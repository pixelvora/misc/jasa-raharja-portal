<!DOCTYPE html>
<html lang="en">
  <?php echo @$_head ?>
  <body class="sidebar-toggle-display <?php if($user->role->id == 2) echo 'sidebar-hidden' ?>">
    <div class="container-scroller">
      <?php echo @$_header ?>
      <div class="container-fluid page-body-wrapper">
        <?php echo @$_sidebar ?>
        <div class="main-panel">
          <div class="content-wrapper">
            <?php echo @$_body ?>
          </div>
        </div>
      </div>
    </div>
    <?php echo @$_foot ?>
  </body>
</html>