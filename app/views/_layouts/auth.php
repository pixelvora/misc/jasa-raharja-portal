<!DOCTYPE html>
<html lang="en">
  <?php echo @$_head ?>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row w-100">
            <div class="col-lg-8 mx-auto">
              <div class="row">
                <div class="col-lg-6 bg-white">
                  <div class="auth-form-light text-left p-5">
                    <!-- <h2>Tabook Admin</h2> -->
                    <img class="w-100 mb-2" src="<?php echo assets_url('img/jasa-raharja-workdesk-logo.png') ?>" alt="">
                    <!-- <h4 class="font-weight-light">Admin Dashboard</h4> -->
                    <?php echo @$_body ?>
                  </div>
                </div>
                <div class="col-lg-6 login-half-bg d-flex flex-row">
                  <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018 Jasa Raharja. All rights reserved.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    
    <?php echo @$_scripts ?>
  </body>
</html>