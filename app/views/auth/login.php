<form class="pt-5" method="POST" action="<?php echo base_url('auth/actionLogin') ?>">
  
  <p>Login menggunakan Akun Anda</p>
  
  <?php if($this->session->flashdata('alert')) echo $this->session->flashdata('alert'); ?>

  <div class="form-group">
    <label>Username / Email</label>
    <input type="text" name="username" class="form-control" placeholder="Username/Email" required>
    <i class="mdi mdi-account"></i>
  </div>

  <div class="form-group">
    <label>Password</label>
    <input type="password" name="password" class="form-control" placeholder="Password" required>
    <i class="mdi mdi-eye"></i>
  </div>

  <div class="mt-5">
    <button type="submit" class="btn btn-block btn-success btn-lg font-weight-medium">Masuk</button>
  </div>

  <!-- <div class="mt-3 text-center">
    <a href="#" class="auth-link text-black">Forgot password?</a>
  </div>  --> 

</form>