<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <?php $this->load->view('_partials/header/headerBrand.partial.php', @$data, FALSE); ?>
  <div class="navbar-menu-wrapper d-flex align-items-center">
    <?php if($user->role->id != 2): ?>
        <?php $this->load->view('_partials/header/headerNavLeft.partial.php', @$data, FALSE); ?>
    <?php endif; ?>
    <?php $this->load->view('_partials/header/headerNavRight.partial.php', @$data, FALSE); ?>
  </div>
</nav>