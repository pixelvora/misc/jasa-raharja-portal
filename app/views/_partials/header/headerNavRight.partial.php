<ul class="navbar-nav navbar-nav-right header-links d-none d-md-flex">
  <li class="nav-item">
    <a class="nav-link pr-0 pl-0">
      <i class="mdi mdi-account"></i> Login Sebagai: <?php echo $user->name ?> (<?php echo $user->role->name ?>)
    </a>
  </li>
  <li class="nav-item">
    <a href="<?php echo base_url('auth/actionLogout') ?>" class="nav-link">
      <i class="mdi mdi-power"></i>Logout
    </a>
  </li>
</ul>

