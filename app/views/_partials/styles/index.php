<?php css('star-admin/vendor/mdi/css/materialdesignicons.min.css') ?>
<?php css('star-admin/vendor/simple-line-icons/css/simple-line-icons.css') ?>
<?php css('star-admin/vendor/flag-icon-css/css/flag-icon.min.css') ?>
<?php css('star-admin/vendor/perfect-scrollbar/css/perfect-scrollbar.css') ?>
<?php css('star-admin/vendor/ti-icons/css/themify-icons.css') ?>
<?php css('star-admin/vendor/font-awesome/css/font-awesome.min.css') ?>


<?php 
if(@$styles['plugins']) {
  foreach($styles['plugins'] as $plugin) {
    css($plugin);
  }
}
?>

<?php css('star-admin/css/style.css'); ?>
<?php css('css/main.css'); ?>
