<?php js('star-admin/vendor/jquery/dist/jquery.min.js') ?>
<?php js('star-admin/vendor/popper.js/dist/umd/popper.min.js') ?>
<?php js('star-admin/vendor/bootstrap/dist/js/bootstrap.min.js') ?>
<?php js('star-admin/vendor/perfect-scrollbar/dist/perfect-scrollbar.min.js') ?>

<?php 
if(@$scripts['plugins']) {
  foreach($scripts['plugins'] as $plugin) {
    js($plugin);
  }
}
?>

<?php js('star-admin/js/off-canvas.js') ?>
<?php js('star-admin/js/hoverable-collapse.js') ?>
<?php js('star-admin/js/misc.js') ?>
<?php js('star-admin/js/settings.js') ?>
<?php js('star-admin/js/todolist.js') ?>

<?php js('js/main.js') ?>
