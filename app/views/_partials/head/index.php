<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    <?php echo (@$title) ? @$title . " - " : "" ?>
    <?php echo @$app_name ?>
  </title>
  <?php echo @$_styles ?>
</head>