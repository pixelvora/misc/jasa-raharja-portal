<?php foreach ($menus as $key => $menu): ?>
  <li class="nav-item <?php if($menu->id == $cur) echo 'active' ?>"> 
    <?php if($menu->children): ?>
    <a class="nav-link" data-toggle="collapse" href="#<?php echo $menu->id ?>" aria-expanded="false" aria-controls="<?php echo $menu->id ?>">
    <?php else: ?>
    <a class="nav-link" href="<?php echo base_url($menu->path) ?>">
    <?php endif; ?>
      <i class="menu-icon icon-sm <?php echo $menu->icon ?>"></i>
      <span class="menu-title"><?php echo $menu->name ?></span>
      <?php if($menu->children): ?>
        <i class="menu-arrow"></i>
      <?php endif; ?>
    </a>
    <?php if($menu->children): ?>
      <div class="collapse" id="<?php echo $menu->id ?>">
        <ul class="nav flex-column sub-menu">
          <?php foreach($menu->children as $submenu): ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo $submenu->path ?>">
                <!-- <i class="<?php echo $submenu->icon ?>"></i>  -->
                <?php echo $submenu->name ?>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php endif; ?>
  </li>
<?php endforeach; ?>