<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav pt-4">
    <?php $this->load->view('_partials/sidebar/sidebarNavMenu.partial.php', @$data, FALSE); ?>
  </ul>
</nav>