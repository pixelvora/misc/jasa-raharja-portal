<div class="row grid-margin">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <h2>Aplikasi Jasa Raharja</h2>
            <p class="text-muted">List Aplikasi & Website Jasa Raharja</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="row portfolio-grid">
              <?php foreach($apps as $app): ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                  <a target="_blank" href="<?php echo $app->url ?>">
                    <figure class="effect-text-in">
                      <img src="<?php echo ($app->image) ? $app->image->url : placeholder(250,250) ?>" alt="image">
                      <figcaption>
                        <h4><?php echo $app->name ?></h4>
                        <p><?php echo $app->url ?></p>
                      </figcaption>     
                    </figure>
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>