<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Template {

    protected $_ci;

    function __construct() {
        $this->_ci = &get_instance();
    }

    function render( $template = NULL, $data = NULL ) {
        $this->data = $data;
        if( $template != NULL )
        $data['_body']      = $this->_ci->load->view($template, $data, TRUE);
        $data['_styles']    = $this->_ci->load->view('_partials/styles/index', $data, TRUE);
        $data['_scripts']   = $this->_ci->load->view('_partials/scripts/index', $data, TRUE);
        $data['_head']      = $this->_ci->load->view('_partials/head/index', $data, TRUE);
        $data['_header']    = $this->_ci->load->view('_partials/header/index', $data, TRUE);
        $data['_foot']      = $this->_ci->load->view('_partials/foot/index', $data, TRUE);
        $data['_footer']    = $this->_ci->load->view('_partials/footer/index', $data, TRUE);
        $data['_sidebar']   = $this->_ci->load->view('_partials/sidebar/index', $data, TRUE);
        echo $this->_ci->load->view('_layouts/default', $data, TRUE);
    }

    function render_auth( $template = NULL, $data = NULL ) {
        if( $template != NULL )
        $data['_body']      = $this->_ci->load->view($template, $data, TRUE);
        $data['_styles']    = $this->_ci->load->view('_partials/styles/index', $data, TRUE);
        $data['_scripts']   = $this->_ci->load->view('_partials/scripts/index', $data, TRUE);
        $data['_head']      = $this->_ci->load->view('_partials/head/index', $data, TRUE);
        echo $this->_ci->load->view('_layouts/auth', $data, TRUE);
    } 

}

?>
