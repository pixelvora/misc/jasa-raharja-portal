<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class News extends JR_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->set_cur('manage/news');
        $this->load->model('M_News', 'News');
    }

    public function view_table()
    {
        $this->init_datatables_assets();
        $response = $this->News->Get();
        if($response) {
            $data['news'] = $response;
        } else {
            $data['news'] = [];
        }
        $this->template->render($this->cur . '/view/index', $data);
    }

    public function view_form($id = null)
    {
        if($id == null) {
            $data['action'] = 'create';
        } else {
            $data['id'] = $id;
            $data['news'] = $this->News->Get($id);
            $data['action'] = 'update';
        }

        $this->template->render($this->cur . '/form/index', $data);
    }

    public function actionCreate()
    {
        $data['id'] = Uuid::uuid1()->toString();
        $data['title'] = $this->input->post('title');
        $data['content'] = $this->input->post('content');
        $data['created_at'] = $this->get_current_timestamp();
        $data['created_by'] = $this->get_user_session()->id;

        $response = $this->News->Create($data);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses menambahkan berita baru'
                ],
                'error' => [
                    'path' => $this->cur . '/form/'
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

    public function actionUpdate()
    {   
        $id = $this->input->post('id');
        $data['title'] = $this->input->post('title');
        $data['content'] = $this->input->post('content');
        $data['updated_at'] = $this->get_current_timestamp();
        $data['updated_by'] = $this->get_user_session()->id;

        $response = $this->News->Update($data, $id);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses mengupdate Berita'
                ],
                'error' => [
                    'path' => $this->cur . '/form/' . $id
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

    public function actionDelete($id)
    {
        $response = $this->News->Delete($id);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses menghapus Berita'
                ],
                'error' => [
                    'msg' => 'Gagal Menghapus Berita'
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

}

/* End of file Apps.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/controllers/Apps.php */