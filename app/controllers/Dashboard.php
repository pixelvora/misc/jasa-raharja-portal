<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends JR_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->set_cur('dashboard');
        $this->load->model('M_App', 'App');
        $this->load->model('M_News', 'News');
    }

    public function viewDashboard()
    {
        $data['title'] = 'Dashboard';
        $data['apps'] = $this->App->Get();
        $data['news'] = $this->News->Get();
        // debug($data);
        $this->template->render('dashboard/view/index', $data);
    }

    public function viewNews($newsId)
    {
        $data['news'] = $this->News->Get($newsId);
        // debug($data);
        $this->template->render('dashboard/view/news-detail', $data);
    }

}

/* End of file Dashboard.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/controllers/Dashboard.php */