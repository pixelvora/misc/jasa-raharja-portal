<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class Users extends JR_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->set_cur('manage/users');
        $this->load->model('M_User', 'User');
    }

    public function view_table()
    {
        $this->init_datatables_assets();
        $data['users'] = $this->User->Get('role', 1);
        $this->template->render($this->cur . '/view/index', $data);
    }

    public function view_form()
    {
        $data['action'] = 'create';
        $this->template->render( $this->cur . '/form/index', $data);
    }

    public function actionCreate()
    {
        $data['id'] = Uuid::uuid1()->toString();
        $data['npp'] = $this->input->post('npp');
        $data['created_at'] = $this->get_current_timestamp();
        $data['created_by'] = $this->get_user_session()->id;

        $response = $this->User->Create($data);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses menambahkan user baru'
                ],
                'error' => [
                    'path' => $this->cur . '/form'
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

    public function actionDelete($userId)
    {
        $response = $this->User->Delete($userId);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses menghapus user'
                ],
                'error' => [
                    'path' => $this->cur
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

}

/* End of file Users.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/controllers/Users.php */